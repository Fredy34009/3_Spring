CREATE DATABASE `spring3` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
use spring3;


CREATE TABLE `pasatiempo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pasatiempo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `perfil_pasatiempo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil` int(11) NOT NULL,
  `pasatiempo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `perfil` (`perfil`),
  KEY `pasatiempo` (`pasatiempo`),
  CONSTRAINT `perfil_pasatiempo_ibfk_1` FOREIGN KEY (`perfil`) REFERENCES `perfil` (`id`),
  CONSTRAINT `perfil_pasatiempo_ibfk_2` FOREIGN KEY (`pasatiempo`) REFERENCES `pasatiempo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `perfil` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `perfil` (`perfil`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`perfil`) REFERENCES `perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
