<%-- 
    Document   : registro
    Created on : 11-08-2019, 03:37:35 PM
    Author     : fredy.alfarousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Pasatiempo</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <jsp:include page="index.jsp"/>
                </div>
                <div class="col-4">
                    <form action="create" method="post">
                        <label class="form-check-label">Nombre del pasatiempo</label>
                        <input class="form-control" name="pasatiempo"/>
                        <button class="btn btn-info">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
