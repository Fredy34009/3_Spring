<%-- 
    Document   : perfiles
    Created on : 11-10-2019, 01:26:03 PM
    Author     : FREDY
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="${pageContext.request.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <jsp:include page="index.jsp"/>
                </div>
                <div class="col-12">
                    <table class="table table-hover table-bordered table-dark">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nombre</td>
                                <td>Apellido</td>
                                <td>Usuario</td>
                                <td>Contraseña</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="i" items="${usuarios}">
                                <tr>
                                    <td>${i.id}</td>
                                    <td>${i.perfil.nombre}</td>
                                    <td>${i.perfil.apellido}</td>
                                    <td>${i.usuario}</td>
                                    <td>${i.pass}</td>
                                    <td><a class="btn btn-info" href="pasatiempo?d=${i.id}&name=${i.perfil.nombre}">Añadir Pasatiempo</a></td>
                                    <td><a class="btn btn-info" href="pasatiempoRead?d=${i.id}&name=${i.perfil.nombre}">Ver tus Pasatiempos</a></td>
                                </tr>
                            </tbody>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
