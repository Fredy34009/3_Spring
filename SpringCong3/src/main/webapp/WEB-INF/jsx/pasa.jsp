<%-- 
    Document   : pasa
    Created on : 11-10-2019, 02:38:15 PM
    Author     : FREDY
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <jsp:include page="index.jsp"/>
                </div>
                <div class="col-8">
                    <form action="pasatiempo" method="post">
                        <label class="form-check-label">Pasatiempo</label>
                        <input class="form-control" type="hidden" name="p" value="${d}" />
                        <label class="form-control">Hola ${name} ¿Cual es tu pasatiempo?</label>
                        <select class="form-control" name="pa">
                            <c:forEach items="${pasatiempos}" var="p">
                                <option value="${p.id}">${p.pasatiempo}</option>
                            </c:forEach>
                        </select>
                        <button class="btn btn-info">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
