<%-- 
    Document   : pasatiempos
    Created on : 11-08-2019, 04:06:10 PM
    Author     : fredy.alfarousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Pasatiempos</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <jsp:include page="index.jsp"/>
                </div>
                <div class="col-12">
                    <table class="table table-responsive table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Pasatiempos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <c:forEach var="pas" items="${pasatiempos}">
                                    <td>${pas.id}</td>
                                    <td>${pas.pasatiempo}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </body>
</html>
