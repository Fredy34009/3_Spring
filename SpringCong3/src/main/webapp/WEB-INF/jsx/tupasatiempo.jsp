<%-- 
    Document   : tupasatiempo
    Created on : 11-10-2019, 03:52:37 PM
    Author     : FREDY
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <jsp:include page="index.jsp"/>
                </div>
                <div class="col-8">
                    <h2><b>${name}</b>  Esta es tu lista de pasatiempos</h2> 
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${ppasatiempos}" var="p">
                                <tr>
                                    <td>${p.pasatiempo.pasatiempo}</td>
                                </tr></c:forEach>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </body>
</html>
