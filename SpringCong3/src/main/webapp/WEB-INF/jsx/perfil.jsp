<%-- 
    Document   : perfil
    Created on : 11-08-2019, 04:35:11 PM
    Author     : fredy.alfarousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <jsp:include page="index.jsp"/>
                </div>
                <div class="col-8">
                    <form action="perfil" method="post">
                        <label>Nombre</label>
                        <input class="form-control" name="nombre"/>
                        <label>Apellido</label>
                        <input class="form-control" name="apellido"/>
                        <label>Usuario</label>
                        <input class="form-control" name="usuario"/>
                        <label>Contraseña</label>
                        <input class="form-control" name="pass"/>
                        <button class="btn btn-dark">Guardar</button>
                    </form>
                    ${error}
                </div>
            </div>
        </div>

    </body>
</html>
