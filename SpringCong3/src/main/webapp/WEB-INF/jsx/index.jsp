<%-- 
    Document   : index
    Created on : 11-08-2019, 03:50:11 PM
    Author     : fredy.alfarousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="${pageContext.request.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <a class="btn btn-info" href="create">Registrar PasaTiempo</a>
        <a class="btn btn-info" href="read">Lista PasaTiempos</a>
        <a class="btn btn-info" href="perfil">Nuevo Usuario</a>
        <a class="btn btn-info" href="perfiles">Usuarios</a>
        <br/><br/><br/>
    </body>
</html>
