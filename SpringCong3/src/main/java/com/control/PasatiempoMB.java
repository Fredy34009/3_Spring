/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control;

import com.imp.PerfilImp;
import com.imp.PerfilPasatiempoImp;
import com.modelo.Pasatiempo;
import com.modelo.Perfil;
import com.modelo.PerfilPasatiempo;
import com.modelo.Usuarios;
import com.util.Dao;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fredy.alfarousam
 */
@Controller
public class PasatiempoMB
{

    @Autowired
    private Dao pasatiempo;
    @Autowired
    private Dao perfil;
    @Autowired
    private Dao usuarios;
    @Autowired
    private PerfilImp perfilImp;
    @Autowired
    private Dao pasatiempos;
    @Autowired
    private PerfilPasatiempoImp pasatiemposImp;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView createPasatiempo(@RequestParam("pasatiempo") String pasatiempo, ModelAndView mav)
    {
        Pasatiempo p = new Pasatiempo();
        p.setPasatiempo(pasatiempo);

        this.pasatiempo.crear(p);
        mav.setViewName("index");
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView createPasa(ModelAndView mav)
    {
        mav.addObject(new Pasatiempo());
        mav.setViewName("registro");
        return mav;
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView index(ModelAndView mav)
    {
        mav.setViewName("index");
        return mav;
    }

    //Metodo que lee 
    @RequestMapping(value = "/read", method = RequestMethod.GET)
    public ModelAndView readPasatiempo(ModelAndView model)
    {

        List<Pasatiempo> pasatiemposs = pasatiempo.read();
        model.addObject("pasatiempos", pasatiemposs);
        model.setViewName("pasatiempos");
        return model;
    }

    @RequestMapping(value = "/perfil", method = RequestMethod.GET)
    public ModelAndView perfil(ModelAndView model)
    {
        model.addObject(new Perfil());
        model.addObject(new Usuarios());
        model.setViewName("perfil");
        return model;
    }

    @RequestMapping(value = "/perfil", method = RequestMethod.POST)
    public ModelAndView perfil(@RequestParam("nombre") String nombre, @RequestParam("apellido") String apellido, @RequestParam("usuario") String user, @RequestParam("pass") String pass, ModelAndView model) throws SQLException
    {
        Perfil p = new Perfil();
        p.setNombre(nombre);
        p.setApellido(apellido);

        List<Usuarios> list = usuarios.read();

        for (Usuarios us : list)
        {
            if (us.getUsuario().equals(user))
            {
                model.addObject("error", "Error este usuario ya existe");
                model.setViewName("perfil");
                return model;
            }
        }
        perfil.crear(p);
        int u = perfilImp.ultimo();
        usuario(u, user, pass);
        model.addObject("error", "Ok registro completo");
        model.setViewName("perfil");
        return model;
    }

    //Ingresa el usuario de acuerdo al perfil
    public void usuario(int u, String user, String pass)
    {
        Usuarios use = new Usuarios();
        use.setUsuario(user);
        use.setPass(pass);
        Perfil p = new Perfil();
        p.setId(u);
        use.setPerfil(p);
        this.usuarios.crear(use);
    }

    @RequestMapping(value = "/perfiles", method = RequestMethod.GET)
    public ModelAndView perfiles(ModelAndView model)
    {
        List<Usuarios> list = usuarios.read();
        model.addObject("usuarios", list);
        model.setViewName("perfiles");
        return model;
    }

    @RequestMapping(value = "/pasatiempo", method = RequestMethod.GET)
    public ModelAndView creAndView(ModelAndView mav, @RequestParam("d") int d, @RequestParam("name") String name)
    {
        mav.addObject(new PerfilPasatiempo());
        List<Pasatiempo> l = pasatiempo.read();
        mav.addObject("pasatiempos", l);
        mav.addObject("d", d);
        mav.addObject("name", name);
        mav.setViewName("pasa");
        return mav;
    }

    @RequestMapping(value = "/pasatiempo", method = RequestMethod.POST)
    public ModelAndView view(ModelAndView mav, @RequestParam("p") int d, @RequestParam("pa") int pa)
    {
        Perfil p = new Perfil();
        p.setId(d);
        Pasatiempo p1 = new Pasatiempo();
        p1.setId(pa);

        PerfilPasatiempo pp = new PerfilPasatiempo();
        pp.setPerfil(p);
        pp.setPasatiempo(p1);
        pasatiempos.crear(pp);
        mav.setViewName("index");
        return mav;
    }

    @RequestMapping(value = "/pasatiempoRead", method = RequestMethod.GET)
    public ModelAndView readPasatiempo(ModelAndView mav, @RequestParam("d") int d, @RequestParam("name") String name)
    {
        mav.addObject(new PerfilPasatiempo());
        List<PerfilPasatiempo> l = pasatiemposImp.readxId(d);
        mav.addObject("ppasatiempos", l);
        mav.addObject("name", name);
        mav.setViewName("tupasatiempo");
        return mav;
    }
}
