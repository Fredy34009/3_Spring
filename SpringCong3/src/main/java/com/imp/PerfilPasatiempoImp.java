/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.modelo.Pasatiempo;
import com.modelo.Perfil;
import com.modelo.PerfilPasatiempo;
import com.util.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fredy.alfarousam
 */
@Repository
public class PerfilPasatiempoImp implements Dao<PerfilPasatiempo>
{

    @Autowired
    Connection dataSource;
    private PreparedStatement p;

    private PerfilPasatiempo pasatiempo;
    private List<PerfilPasatiempo> pasatiempos;
    private final String[] ar =
    {
        "insert into perfil_pasatiempo(perfil,pasatiempo) values(?,?)",
        "select * from perfil_pasatiempo",
        "select pa.pasatiempo from perfil_pasatiempo pp INNER join pasatiempo pa on pa.id=pp.pasatiempo where perfil=?"
    };

    @Override
    public void crear(PerfilPasatiempo pp)
    {
        try
        {
            Pasatiempo pasatiempo1 = pp.getPasatiempo();
            Perfil perfil = pp.getPerfil();
            p = dataSource.prepareStatement(ar[0]);

            p.setInt(1, perfil.getId());
            p.setInt(2, pasatiempo1.getId());
            
            p.executeUpdate();
        } catch (SQLException e)
        {
            System.out.println("Error en Perfil Pasatiempo: Al crear");
        } finally
        {
            System.out.println("Recuerde cerrar la conexion");
        }
    }

    @Override
    public List<PerfilPasatiempo> read()
    {
        try
        {
            p = dataSource.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            pasatiempos = new ArrayList<>();
            Perfil p;
            Pasatiempo p1;

            while (rs.next())
            {
                p = new Perfil();
                p1 = new Pasatiempo();
                
                pasatiempo = new PerfilPasatiempo();
                pasatiempo.setId(rs.getInt("id"));
                pasatiempo.setPasatiempo(p1);
                pasatiempo.setPerfil(p);
                pasatiempos.add(pasatiempo);
            }
            return pasatiempos;
        } catch (Exception e)
        {
            return null;
        }
    }
    public List<PerfilPasatiempo> readxId(int id)
    {
        try
        {
            p = dataSource.prepareStatement(ar[2]);
            p.setInt(1, id);
            ResultSet rs = p.executeQuery();
            pasatiempos = new ArrayList<>();
            Pasatiempo p1;

            while (rs.next())
            {
                pasatiempo = new PerfilPasatiempo();
                p1 = new Pasatiempo();
                p1.setPasatiempo(rs.getString("pa.pasatiempo"));
                pasatiempo.setPasatiempo(p1);
                pasatiempos.add(pasatiempo);
            }
            return pasatiempos;
        } catch (Exception e)
        {
            return null;
        }
    }
}
