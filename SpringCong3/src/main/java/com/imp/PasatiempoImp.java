/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.modelo.Pasatiempo;
import com.util.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fredy.alfarousam
 */
@Repository
public class PasatiempoImp implements Dao<Pasatiempo> {

    @Autowired
    Connection dataSource;
    private PreparedStatement p;

    private Pasatiempo pasatiempo;
    private List<Pasatiempo> pasatiempos;
    private final String[] ar = {
        "insert into pasatiempo(pasatiempo) values(?)",
        "select * from pasatiempo"
    };

    public PasatiempoImp() {
    }

    @Override
    public void crear(Pasatiempo pasatiempo) {
        try {
            p = dataSource.prepareStatement(ar[0]);
            p.setString(1, pasatiempo.getPasatiempo());
            p.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error en Pasatiempo: Al crear");
        } finally {
            System.out.println("Recuerde cerrar la conexion");
        }
    }

    public List<Pasatiempo> read() {
        try {
            p = dataSource.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            pasatiempos = new ArrayList<Pasatiempo>();
            while (rs.next()) {
                pasatiempo = new Pasatiempo();
                pasatiempo.setId(rs.getInt("id"));
                pasatiempo.setPasatiempo(rs.getString("pasatiempo"));
                pasatiempos.add(pasatiempo);
            }
            return pasatiempos;
        } catch (Exception e) {
            return null;
        }
    }
}
