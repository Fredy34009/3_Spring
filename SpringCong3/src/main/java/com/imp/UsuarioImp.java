/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.modelo.Perfil;
import com.modelo.Usuarios;
import com.util.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioImp implements Dao<Usuarios> {

    @Autowired
    Connection conexion;
    private PreparedStatement p;
    private Usuarios usuario;
    private List<Usuarios> usuarios;

    private final String[] ar = {
        "insert into usuarios(usuario,pass,perfil) value(?,?,?)",
        "select * from usuarios INNER join perfil on perfil.id=usuarios.id"
    };

    @Override
    public void crear(Usuarios t) {
        try {
            Perfil perfil = new Perfil();
            perfil = t.getPerfil();
            p = conexion.prepareStatement(ar[0]);
            p.setString(1, t.getUsuario());
            p.setString(2, t.getPass());
            p.setInt(3, perfil.getId());
            p.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error en Usuario: Al crear");
        } finally {
            System.out.println("Recuerde Usuario la conexion");
        }
    }

    @Override
    public List<Usuarios> read() {
        try {
            p = conexion.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            usuarios = new ArrayList<>();
            Perfil perfil;
            while (rs.next()) {
                usuario = new Usuarios();
                perfil = new Perfil();
                perfil.setId(rs.getInt("id"));
                perfil.setNombre(rs.getString("nombre"));
                perfil.setApellido(rs.getString("apellido"));
                usuario.setId(rs.getInt("id"));
                usuario.setUsuario(rs.getString("usuario"));
                usuario.setPass(rs.getString("pass"));
                usuario.setPerfil(perfil);
                usuarios.add(usuario);
            }
            return usuarios;
        } catch (Exception e) {
            return null;
        }
    }

}
