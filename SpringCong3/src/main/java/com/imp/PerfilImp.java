/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.modelo.Perfil;
import com.util.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fredy.alfarousam
 */
@Repository
public class PerfilImp implements Dao<Perfil> {

    @Autowired
    Connection conexion;
    private PreparedStatement p;
    private Perfil perfil;
    private List<Perfil> perfiles;
    private final String[] ar = {
        "insert into perfil(nombre,apellido) values(?,?)",
        "select * from perfil",
        "select id from perfil"
    };

    @Override
    public void crear(Perfil perfil) {
        try {
            p = conexion.prepareStatement(ar[0]);
            p.setString(1, perfil.getNombre());
            p.setString(2, perfil.getApellido());
            p.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error en Pasatiempo: Al crear");
        } finally {
            System.out.println("Recuerde cerrar la conexion");
        }
    }

    @Override
    public List<Perfil> read() {
        try {
            p = conexion.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            perfiles = new ArrayList<>();
            while (rs.next()) {
                perfil = new Perfil();
                perfil.setId(rs.getInt("id"));
                perfil.setNombre(rs.getString("nombre"));
                perfil.setApellido(rs.getString("apellido"));
                perfiles.add(perfil);
            }
            return perfiles;
        } catch (Exception e) {
            return null;
        }
    }
    
    public int ultimo()
    {
        try {
            p=conexion.prepareStatement(ar[2]);
            ResultSet rs = p.executeQuery();
            int i=0;
            while (rs.next()) {
                i=rs.getInt("id");
            }
            System.out.println("Numero ingreasado "+i);
            return i;
        } catch (Exception e) {
            return 0;
        }
        
    }
}
