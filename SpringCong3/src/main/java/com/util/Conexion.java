/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

/**
 *
 * @author fredy.alfarousam
 */
import com.imp.PasatiempoImp;
import com.imp.PerfilImp;
import com.imp.PerfilPasatiempoImp;
import com.imp.UsuarioImp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan("com")
public class Conexion {
    @Bean
    InternalResourceViewResolver viewRes() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsx/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
     @Bean
    public Connection getConex(){
        Connection c;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/spring3?useSSL=false", "root", "root");
            System.out.println("Exito en la conexion");
        } catch (SQLException e) {
            c = null;
        }
        catch(ClassNotFoundException cx){
            c = null;
        }
        return c;
    }
    @Bean
    public Dao pasatiempo() {
        return new PasatiempoImp();
    }
    @Bean
    public Dao perfil()
    {
        return new PerfilImp();
    }
    @Bean
    public Dao usuarios()
    {
        return new UsuarioImp();
    }
    @Bean PerfilImp usuarioImp()
    {
        return new PerfilImp();
    }
    @Bean Dao pasatiempos()
    {
        return new PerfilPasatiempoImp();
    }
    
    @Bean PerfilPasatiempoImp pasatiemposImp()
    {
        return new PerfilPasatiempoImp();
    }
}
