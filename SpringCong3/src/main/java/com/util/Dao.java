/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import com.modelo.Pasatiempo;
import java.util.List;

/**
 *
 * @author fredy.alfarousam
 */
public interface Dao <T> {
    public void crear(T t);
    public List<T> read();
}
